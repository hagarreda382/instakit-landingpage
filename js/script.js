// sticky header
$(document).ready(function () {
    if ($("#not-sticky").length != 0) {
      return;
    }
    var $sticky = $("header");
    if ($sticky.length === 0) return;
    var stickyOffsetTop = $sticky.offset().top;
  
    $(window).scroll(function (e) {
      e.preventDefault();
  
      var scrollTop = $(window).scrollTop();
        if (scrollTop > stickyOffsetTop) {
          $sticky.addClass("sticky-navbar");
        } else {
          $sticky.removeClass("sticky-navbar");
        }
     
    });
  });
/*************scroll ************/
let scroll = document.querySelector(".scroll");
let isDown = false;
let scrollX;
let scrollLeft;

// Mouse Up Function
scroll &&
  scroll.addEventListener("mouseup", () => {
    isDown = false;
    scroll.classList.remove("active");
  });

// Mouse Leave Function
scroll &&
  scroll.addEventListener("mouseleave", () => {
    isDown = false;
    scroll.classList.remove("active");
  });

// Mouse Down Function
scroll &&
  scroll.addEventListener("mousedown", (e) => {
    e.preventDefault();
    isDown = true;
    scroll.classList.add("active");
    scrollX = e.pageX - scroll.offsetLeft;
    scrollLeft = scroll.scrollLeft;
  });

// Mouse Move Function
scroll &&
  scroll.addEventListener("mousemove", (e) => {
    if (!isDown) return;
    e.preventDefault();
    var element = e.pageX - scroll.offsetLeft;
    var scrolling = (element - scrollX) * 2;
    scroll.scrollLeft = scrollLeft - scrolling;
  });  
  //==============================
